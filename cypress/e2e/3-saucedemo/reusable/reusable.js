function login(username, password) {
    cy.visit('https://www.saucedemo.com/')
    cy.get('#user-name').clear()
    cy.get('#password').clear()
    cy.get('#user-name').type(username)
    cy.get('#password').type(password)
    cy.get('#login-button').click()
    cy.get('.app_logo')
    .should('be.visible')
    .should('contain','Swag Labs')
}

function addProduct(){
    cy.get('.shopping_cart_badge').should('not.exist')
    cy.get('#add-to-cart-sauce-labs-backpack').click()
    cy.get('#remove-sauce-labs-backpack').should('contain','Remove')
    let prod;
    cy.get('#item_4_title_link > .inventory_item_name')
    .invoke('text')
    .then((text) => {
      prod = text;
    });
    cy.get('.shopping_cart_badge').should('exist')
    cy.get('.shopping_cart_link').click()
    cy.get('.title')
    .should('be.visible')
    .should('contain','Your Cart')

    cy.get('.inventory_item_name')
    .invoke('text')
    .then((text) => {
      expect(text).to.equal(prod)
    })
}

function logout(){
    cy.get('#react-burger-menu-btn').click()
    cy.get('#logout_sidebar_link').click()
    cy.get('.login_logo')
    .should('be.visible')
    .should('contain','Swag Labs')
}
module.exports = {
    login,
    addProduct,
    logout
};
  
const { afterEach } = require("mocha")
const { login, addProduct, logout } = require('./reusable/reusable');

describe('Login and add product', () => {
    beforeEach(() => {
        let username = 'standard_user'
        let password = 'secret_sauce'
        login(username, password)
        addProduct()
    })
    it('User can not checkout products with blank fields', () => {
        cy.get('[data-test="checkout"]').click()
        cy.get('[data-test="continue"]').click()
        cy.get('[data-test="error"]').should('have.text','Error: First Name is required')
  
        cy.get('[data-test="firstName"]').type('Widya')
        cy.get('[data-test="continue"]').click()
        cy.get('[data-test="error"]').should('have.text','Error: Last Name is required')
  
        cy.get('[data-test="lastName"]').type('Rizka')
        cy.get('[data-test="continue"]').click()
        cy.get('[data-test="error"]').should('have.text','Error: Postal Code is required')
    })
  
    it('User can checkout products', () => {
        cy.get('[data-test="checkout"]').click()
        cy.get('[data-test="firstName"]').type('Widya')
        cy.get('[data-test="lastName"]').type('Rizka')
        cy.get('[data-test="postalCode"]').type('123')
        cy.get('[data-test="continue"]').click()
        cy.get('.title').should('have.text','Checkout: Overview')
        cy.get('[data-test="finish"]').click()
        cy.get('.complete-header').should('have.text','Thank you for your order!')
        cy.get('[data-test="back-to-products"]').click()
        cy.get('.title').should('have.text','Products')
        logout()
    })  
})
const { afterEach } = require("mocha")
const { login, addProduct } = require('./reusable/reusable');

describe('Login', () => {
    beforeEach(() => {
      let username = 'standard_user'
      let password = 'secret_sauce'
      login(username, password);
    })
    it('User can add products', () => {
      cy.get('.shopping_cart_badge').should('not.exist')
      cy.get('#add-to-cart-sauce-labs-backpack').click()
      cy.get('#remove-sauce-labs-backpack').should('contain','Remove')
      let prod;
      cy.get('#item_4_title_link > .inventory_item_name')
      .invoke('text')
      .then((text) => {
        prod = text;
      });
      cy.get('.shopping_cart_badge').should('exist')
      cy.get('.shopping_cart_link').click()
      cy.get('.title')
      .should('be.visible')
      .should('contain','Your Cart')

      cy.get('.inventory_item_name')
      .invoke('text')
      .then((text) => {
        expect(text).to.equal(prod)
      })
    })
  })